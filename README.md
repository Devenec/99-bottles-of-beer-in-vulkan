# 99 Bottles of Beer in Vulkan

A program that generates the lyrics of 99 Bottles of Beer in a [compute shader] on Vulkan.


## About

The shader could have been less messy with `GL_EXT_shader_8bit_storage` extension, but I didn't want to use
extensions.

The program has been tested on Windows 10 with Nvidia GTX 970 and AMD Radeon RX 5700 XT.

Building requires the following Vulkan header files placed in `include/vulkan`:
- `vk_platform.h`
- `vulkan.h`
- `vulkan_core.h`

and `vulkan-1.lib` placed in the root directory. The program was built using [Vulkan SDK 1.2.141].


## Limitations

The program output is correct only on little-endian systems, due to the way the shader packs the lyrics.


## Why?

Why not?


## Copyright & License

Copyright 2020 Eetu 'Devenec' Oinasmaa  
Licensed under MIT License


[compute shader]: shader.comp
[Vulkan SDK 1.2.141]: https://vulkan.lunarg.com/sdk/home
