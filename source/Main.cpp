/**
 * @file Main.cpp

 * 99 Bottles of Beer in Vulkan
 * Copyright 2020 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

#include <algorithm>
#include <array>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iterator>
#include <vector>

#include <vulkan/vulkan.h>

static void checkResult(const VkResult result, const char* context);
static uint32_t getComputeQueueFamily(const std::vector<VkQueueFamilyProperties>& queueFamilyProperties);
static uint32_t getHostMemoryTypeIndex(const VkPhysicalDeviceMemoryProperties& memoryProperties,
	const VkMemoryRequirements& memoryRequirements);

static std::vector<char> loadShaderCode();

int main()
{
	VkApplicationInfo appInfo{};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = "99 Bottles of Beer in Vulkan";
	appInfo.applicationVersion = 1;
	appInfo.pEngineName = "";
	appInfo.engineVersion = 1;
	appInfo.apiVersion = VK_API_VERSION_1_0;

	VkInstanceCreateInfo instanceCreateInfo{};
	instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instanceCreateInfo.pApplicationInfo = &appInfo;
	instanceCreateInfo.enabledExtensionCount = 0;

	VkInstance instance = nullptr;
	VkResult result = vkCreateInstance(&instanceCreateInfo, nullptr, &instance);
	checkResult(result, "vkCreateInstance");

	uint32_t deviceCount = 1;
	VkPhysicalDevice physicalDevice = nullptr;
	result = vkEnumeratePhysicalDevices(instance, &deviceCount, &physicalDevice);
	checkResult(result, "vkEnumeratePhysicalDevices");

	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);
	std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilyProperties.data());
	const float queuePriority = 1.0f;

	VkDeviceQueueCreateInfo queueCreateInfo{};
	queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	queueCreateInfo.queueFamilyIndex = getComputeQueueFamily(queueFamilyProperties);
	queueCreateInfo.queueCount = 1;
	queueCreateInfo.pQueuePriorities = &queuePriority;

	VkDeviceCreateInfo deviceCreateInfo{};
	deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceCreateInfo.queueCreateInfoCount = 1;
	deviceCreateInfo.pQueueCreateInfos = &queueCreateInfo;

	VkDevice device = nullptr;
	result = vkCreateDevice(physicalDevice, &deviceCreateInfo, nullptr, &device);
	checkResult(result, "vkCreateDevice");

	VkQueue queue = nullptr;
	vkGetDeviceQueue(device, queueCreateInfo.queueFamilyIndex, 0, &queue);

	VkCommandPoolCreateInfo commandPoolCreateInfo{};
	commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
	commandPoolCreateInfo.queueFamilyIndex = queueCreateInfo.queueFamilyIndex;

	VkCommandPool commandPool = nullptr;
	result = vkCreateCommandPool(device, &commandPoolCreateInfo, nullptr, &commandPool);
	checkResult(result, "vkCreateCommandPool");

	VkCommandBufferAllocateInfo commandBufferAllocateInfo{};
	commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocateInfo.commandPool = commandPool;
	commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandBufferAllocateInfo.commandBufferCount = 1;

	VkCommandBuffer commandBuffer = nullptr;
	result = vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, &commandBuffer);
	checkResult(result, "vkAllocateCommandBuffers");

	VkBufferCreateInfo bufferCreateInfo{};
	bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferCreateInfo.size = 16 * 1024;
	bufferCreateInfo.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
	bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VkBuffer buffer = nullptr;
	result = vkCreateBuffer(device, &bufferCreateInfo, nullptr, &buffer);
	checkResult(result, "vkCreateBuffer");

	VkPhysicalDeviceMemoryProperties memoryProperties{};
	vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);

	VkMemoryRequirements bufferMemoryRequirements{};
	vkGetBufferMemoryRequirements(device, buffer, &bufferMemoryRequirements);

	VkMemoryAllocateInfo memoryAllocateInfo{};
	memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	memoryAllocateInfo.allocationSize = bufferMemoryRequirements.size;
	memoryAllocateInfo.memoryTypeIndex = getHostMemoryTypeIndex(memoryProperties, bufferMemoryRequirements);

	VkDeviceMemory bufferMemory = nullptr;
	result = vkAllocateMemory(device, &memoryAllocateInfo, nullptr, &bufferMemory);
	checkResult(result, "vkAllocateMemory");

	result = vkBindBufferMemory(device, buffer, bufferMemory, 0);
	checkResult(result, "vkBindBufferMemory");

	const std::vector<char> shaderCode = loadShaderCode();

	VkShaderModuleCreateInfo shaderModuleCreateInfo{};
	shaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	shaderModuleCreateInfo.codeSize = shaderCode.size();
	shaderModuleCreateInfo.pCode = reinterpret_cast<const uint32_t*>(shaderCode.data());

	VkShaderModule shaderModule = nullptr;
	result = vkCreateShaderModule(device, &shaderModuleCreateInfo, nullptr, &shaderModule);
	checkResult(result, "vkCreateShaderModule");

	VkDescriptorSetLayoutBinding descriptorSetLayoutBinding{};
	descriptorSetLayoutBinding.binding = 0;
	descriptorSetLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	descriptorSetLayoutBinding.descriptorCount = 1;
	descriptorSetLayoutBinding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;

	VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo{};
	descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	descriptorSetLayoutCreateInfo.bindingCount = 1;
	descriptorSetLayoutCreateInfo.pBindings = &descriptorSetLayoutBinding;

	VkDescriptorSetLayout descriptorSetLayout = nullptr;
	result = vkCreateDescriptorSetLayout(device, &descriptorSetLayoutCreateInfo, nullptr,
		&descriptorSetLayout);

	checkResult(result, "vkCreateDescriptorSetLayout");

	VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo{};
	pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutCreateInfo.setLayoutCount = 1;
	pipelineLayoutCreateInfo.pSetLayouts = &descriptorSetLayout;

	VkPipelineLayout pipelineLayout = nullptr;
	result = vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, nullptr, &pipelineLayout);
	checkResult(result, "vkCreatePipelineLayout");

	VkPipelineShaderStageCreateInfo shaderStageInfo{};
	shaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	shaderStageInfo.stage = VK_SHADER_STAGE_COMPUTE_BIT;
	shaderStageInfo.module = shaderModule;
	shaderStageInfo.pName = "main";

	VkComputePipelineCreateInfo pipelineCreateInfo{};
	pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
	pipelineCreateInfo.stage = shaderStageInfo;
	pipelineCreateInfo.layout = pipelineLayout;

	VkPipeline pipeline = nullptr;
	result = vkCreateComputePipelines(device, nullptr, 1, &pipelineCreateInfo, nullptr, &pipeline);
	checkResult(result, "vkCreateComputePipelines");

	VkDescriptorPoolSize descriptorPoolSize{};
	descriptorPoolSize.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	descriptorPoolSize.descriptorCount = 1;

	VkDescriptorPoolCreateInfo descriptorPoolCreateInfo{};
	descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	descriptorPoolCreateInfo.maxSets = 1;
	descriptorPoolCreateInfo.poolSizeCount = 1;
	descriptorPoolCreateInfo.pPoolSizes = &descriptorPoolSize;

	VkDescriptorPool descriptorPool = nullptr;
	result = vkCreateDescriptorPool(device, &descriptorPoolCreateInfo, nullptr, &descriptorPool);
	checkResult(result, "vkCreateDescriptorPool");

	VkDescriptorSetAllocateInfo descriptorSetAllocateInfo{};
	descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	descriptorSetAllocateInfo.descriptorPool = descriptorPool;
	descriptorSetAllocateInfo.descriptorSetCount = 1;
	descriptorSetAllocateInfo.pSetLayouts = &descriptorSetLayout;

	VkDescriptorSet descriptorSet = nullptr;
	result = vkAllocateDescriptorSets(device, &descriptorSetAllocateInfo, &descriptorSet);
	checkResult(result, "vkAllocateDescriptorSets");

	VkDescriptorBufferInfo descriptorBufferInfo{};
	descriptorBufferInfo.buffer = buffer;
	descriptorBufferInfo.range = bufferMemoryRequirements.size;

	VkWriteDescriptorSet descriptorWrite{};
	descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	descriptorWrite.dstSet = descriptorSet;
	descriptorWrite.dstBinding = 0;
	descriptorWrite.descriptorCount = 1;
	descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	descriptorWrite.pBufferInfo = &descriptorBufferInfo;

	vkUpdateDescriptorSets(device, 1, &descriptorWrite, 0, nullptr);

	VkCommandBufferBeginInfo commandBufferBeginInfo{};
	commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	result = vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);
	checkResult(result, "vkBeginCommandBuffer");

	vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
	vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipelineLayout, 0, 1,
		&descriptorSet, 0, nullptr);

	vkCmdDispatch(commandBuffer, 100, 1, 1);

	result = vkEndCommandBuffer(commandBuffer);
	checkResult(result, "vkEndCommandBuffer");

	VkSubmitInfo submitInfo{};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;

	result = vkQueueSubmit(queue, 1, &submitInfo, nullptr);
	checkResult(result, "vkQueueSubmit");

	result = vkDeviceWaitIdle(device);
	checkResult(result, "vkDeviceWaitIdle");

	void* bufferData = nullptr;
	result = vkMapMemory(device, bufferMemory, 0, VK_WHOLE_SIZE, 0, &bufferData);
	checkResult(result, "vkMapMemory");
	std::cout << static_cast<char*>(bufferData) << '\n';
	vkUnmapMemory(device, bufferMemory);

	vkDestroyDescriptorPool(device, descriptorPool, nullptr);
	vkDestroyPipeline(device, pipeline, nullptr);
	vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
	vkDestroyDescriptorSetLayout(device, descriptorSetLayout, nullptr);
	vkDestroyShaderModule(device, shaderModule, nullptr);
	vkFreeMemory(device, bufferMemory, nullptr);
	vkDestroyBuffer(device, buffer, nullptr);
	vkDestroyCommandPool(device, commandPool, nullptr);
	vkDestroyDevice(device, nullptr);
	vkDestroyInstance(instance, nullptr);

	return 0;
}

static void checkResult(const VkResult result, const char* context)
{
	if(result != VK_SUCCESS && result != VK_INCOMPLETE)
	{
		std::cout << "Sorry, no beer for you!\n\n" << context << " returned VkResult " << result << ".\n";
		std::exit(-1);
	}
}

static uint32_t getComputeQueueFamily(const std::vector<VkQueueFamilyProperties>& queueFamilyProperties)
{
	auto iterator = std::find_if(queueFamilyProperties.begin(), queueFamilyProperties.end(),
		[](const VkQueueFamilyProperties& queueFamilyProperties)
	{
		return (queueFamilyProperties.queueFlags & VK_QUEUE_COMPUTE_BIT) == VK_QUEUE_COMPUTE_BIT;
	});

	if(iterator == queueFamilyProperties.end())
	{
		std::cout << "Sorry, no beer for you!\n\n" << "Failed to find a compute queue family.\n";
		std::exit(-1);
	}

	return static_cast<uint32_t>(iterator - queueFamilyProperties.begin());
}

static uint32_t getHostMemoryTypeIndex(const VkPhysicalDeviceMemoryProperties& memoryProperties,
	const VkMemoryRequirements& memoryRequirements)
{
	for(uint32_t i = 0; i < memoryProperties.memoryTypeCount; ++i)
	{
		const uint32_t memoryTypeBit = 1 << i;

		if((memoryRequirements.memoryTypeBits & memoryTypeBit) != memoryTypeBit)
			continue;

		const VkMemoryType& memoryType = memoryProperties.memoryTypes[i];

		if((memoryType.propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) !=
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
		{
			continue;
		}

		if((memoryType.propertyFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) !=
			VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)
		{
			continue;
		}

		return i;
	}

	std::cout << "Sorry, no beer for you!\n\n" << "Failed to find suitable host memory type.\n";
	std::exit(-1);
}

static std::vector<char> loadShaderCode()
{
	std::ifstream fileStream("shader.spirv", std::ios::binary | std::ios::in);

	if(!fileStream.is_open())
	{
		std::cout << "Sorry, no beer for you!\n\n" << "Failed to open 'shader.spirv'.\n";
		std::exit(-1);
	}

	return std::vector<char>(std::istreambuf_iterator<char>(fileStream), {});
}
